-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 12.11.2022 11:42:38 UTC

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity tb_L1_T3_LOGIC_EQ is
end tb_L1_T3_LOGIC_EQ;

architecture tb of tb_L1_T3_LOGIC_EQ is

    component L1_T3_LOGIC_EQ
        port (X : in std_logic_vector (6 downto 0);
              y : out std_logic);
    end component;

    signal X : std_logic_vector (6 downto 0) := (others => '0');
    signal y : std_logic;

begin

    dut : L1_T3_LOGIC_EQ
    port map (X => X,
              y => y);

    stimuli : process
    begin
        wait for 2ns;
        X <= X + 1;
    end process;

end tb;