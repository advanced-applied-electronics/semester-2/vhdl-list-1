library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_unsigned.ALL;

entity tb_L1_T4_3to7conv is
end tb_L1_T4_3to7conv;

architecture tb of tb_L1_T4_3to7conv is

    component L1_T4_3to7conv
        port (A : in std_logic_vector (2 downto 0);
             Y : out std_logic_vector (6 downto 0));
    end component;

    signal A : std_logic_vector (2 downto 0) := (others => '0');
    signal Y : std_logic_vector (6 downto 0);

begin

    dut : L1_T4_3to7conv
        port map (A => A,
                 Y => Y);

    stimuli : process
    begin
        wait for 20ns;
        A <= A + 1;
    end process;

end tb;