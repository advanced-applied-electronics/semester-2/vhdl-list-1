library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity tb_L1_T1_3IN_XOR is
end tb_L1_T1_3IN_XOR;

architecture tb of tb_L1_T1_3IN_XOR is

    component L1_T1_3IN_XOR
        port (A : in std_logic;
              B : in std_logic;
              C : in std_logic;
              Y : out std_logic);
    end component;

    signal A : std_logic;
    signal B : std_logic;
    signal C : std_logic;
    signal Y : std_logic;

begin

    dut : L1_T1_3IN_XOR
    port map (A => A,
              B => B,
              C => C,
              Y => Y);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        A <= '0';
        B <= '0';
        C <= '0';

        -- EDIT Add stimuli here
        wait for 100 ns;
        A <= '0';
        B <= '0';
        C <= '1';
        wait for 100 ns;
        A <= '0';
        B <= '1';
        C <= '0';
        wait for 100 ns;
        A <= '0';
        B <= '1';
        C <= '1';
        wait for 100 ns;
        A <= '1';
        B <= '0';
        C <= '0';
        wait for 100 ns;
        A <= '1';
        B <= '0';
        C <= '1';
        wait for 100 ns;
        A <= '1';
        B <= '1';
        C <= '0';
        wait for 100 ns;
        A <= '1';
        B <= '1';
        C <= '1';

        wait;
    end process;

end tb;