library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.ALL;

entity tb_L1_T6_add_subtract is
end tb_L1_T6_add_subtract;

architecture tb of tb_L1_T6_add_subtract is

    component L1_T6_add_subtract
        port (A : in std_logic_vector (10 downto 0);
              B : in std_logic_vector (10 downto 0);
              X : in std_logic;
              Y : out std_logic_vector (12 downto 0));
    end component;

    signal A : std_logic_vector (10 downto 0);
    signal B : std_logic_vector (10 downto 0);
    signal X : std_logic;
    signal Y : std_logic_vector (12 downto 0);

begin

    dut : L1_T6_add_subtract
    port map (A => A,
              B => B,
              X => X,
              Y => Y);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        A <= std_logic_vector(to_unsigned(21, 11));
        B <= std_logic_vector(to_unsigned(25, 11));
        X <= '0';

        -- EDIT Add stimuli here
        wait for 10ns;
        X <= '1';
        
        wait for 10ns;
        X <= '0';
        A <= (others => '1');
        B <= (others => '1');

        wait;
    end process;

end tb;
