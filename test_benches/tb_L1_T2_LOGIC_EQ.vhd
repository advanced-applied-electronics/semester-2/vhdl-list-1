library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity tb_L1_T2_LOGIC_EQ is
end tb_L1_T2_LOGIC_EQ;

architecture tb of tb_L1_T2_LOGIC_EQ is

    component L1_T2_LOGIC_EQ
        port (X : in std_logic_vector (3 downto 0);
              Y : out std_logic);
    end component;

    signal X : std_logic_vector (3 downto 0) := "0000";
    signal Y : std_logic;

begin

    dut : L1_T2_LOGIC_EQ
    port map (X => X,
              Y => Y);

    stimuli : process
    begin
        wait for 60ns;
        X <= X + 1;
    end process;

end tb;