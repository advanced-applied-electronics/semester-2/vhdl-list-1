library ieee;
use ieee.std_logic_1164.all;

entity tb_AND_1 is
end tb_AND_1;

architecture tb of tb_AND_1 is

    component AND_1
        port (a : in std_logic;
              b : in std_logic;
              y : out std_logic);
    end component;

    signal a : std_logic;
    signal b : std_logic;
    signal y : std_logic;

begin

    dut : AND_1
    port map (a => a,
              b => b,
              y => y);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        a <= '0';
        b <= '0';

        -- EDIT Add stimuli here
        wait for 100 ns;
        a <= '1';
        wait for 100 ns;
        b <= '1';

        wait;
    end process;

end tb;