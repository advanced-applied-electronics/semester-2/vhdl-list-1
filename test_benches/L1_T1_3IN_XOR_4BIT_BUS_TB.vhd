library ieee;
use ieee.std_logic_1164.all;

entity tb_L1_T1_3IN_XOR_4BIT_BUS is
end tb_L1_T1_3IN_XOR_4BIT_BUS;

architecture tb of tb_L1_T1_3IN_XOR_4BIT_BUS is

    component L1_T1_3IN_XOR_4BIT_BUS
        port (A : in std_logic_vector (3 downto 0);
              B : in std_logic_vector (3 downto 0);
              C : in std_logic_vector (3 downto 0);
              Y : out std_logic_vector (3 downto 0));
    end component;

    signal A : std_logic_vector (3 downto 0);
    signal B : std_logic_vector (3 downto 0);
    signal C : std_logic_vector (3 downto 0);
    signal Y : std_logic_vector (3 downto 0);

begin

    dut : L1_T1_3IN_XOR_4BIT_BUS
    port map (A => A,
              B => B,
              C => C,
              Y => Y);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        A <= (others => '0');
        B <= (others => '0');
        C <= (others => '0');

        -- EDIT Add stimuli here
        wait for 100 ns;
        A <= "0101";
        wait for 100 ns;
        B <= "1010";
        wait for 100 ns;
        C <= (others => '1');

        wait;
    end process;

end tb;