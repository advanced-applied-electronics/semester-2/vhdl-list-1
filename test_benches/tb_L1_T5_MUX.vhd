-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 12.11.2022 13:36:47 UTC

library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_unsigned.ALL;

entity tb_L1_T5_MUX is
end tb_L1_T5_MUX;

architecture tb of tb_L1_T5_MUX is

    component L1_T5_MUX
        port (A   : in std_logic;
              B   : in std_logic;
              C   : in std_logic;
              D   : in std_logic;
              SEL : in std_logic_vector (1 downto 0);
              Y   : out std_logic);
    end component;

    signal A   : std_logic;
    signal B   : std_logic;
    signal C   : std_logic;
    signal D   : std_logic;
    signal SEL : std_logic_vector (1 downto 0);
    signal Y   : std_logic;

begin

    dut : L1_T5_MUX
    port map (A   => A,
              B   => B,
              C   => C,
              D   => D,
              SEL => SEL,
              Y   => Y);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        A <= '0';
        B <= '1';
        C <= '0';
        D <= '1';
        SEL <= (others => '0');

        -- EDIT Add stimuli here
        for i in 1 to 4 loop
            wait for 20 ns;
            SEL <= SEL + 1;
            end loop;

        wait;
    end process;

end tb;