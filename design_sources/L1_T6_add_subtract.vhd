----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/17/2022 11:12:23 AM
-- Design Name: 
-- Module Name: L1_T6_add_subtract - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity L1_T6_add_subtract is
    Port ( A : in STD_LOGIC_VECTOR (10 downto 0);
           B : in STD_LOGIC_VECTOR (10 downto 0);
           X : in STD_LOGIC;
           Y : out STD_LOGIC_VECTOR (12 downto 0));
end L1_T6_add_subtract;

architecture Behavioral of L1_T6_add_subtract is

signal signed_A: integer;
signal signed_B: integer;

begin
signed_A <= to_integer(unsigned(A));
signed_B <= to_integer(unsigned(B));
with X select Y <=
     std_logic_vector(to_signed((signed_A + signed_B), 13)) when '0',
     std_logic_vector(to_signed((signed_A - signed_B), 13)) when '1',
     (others=>'0') when others;


end Behavioral;
