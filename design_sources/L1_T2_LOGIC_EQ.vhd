----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/10/2022 10:21:04 AM
-- Design Name: 
-- Module Name: L1_T2_LOGIC_EQ - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity L1_T2_LOGIC_EQ is
    Port ( X : in STD_LOGIC_VECTOR (3 downto 0);
           Y : out STD_LOGIC);
end L1_T2_LOGIC_EQ;

architecture Behavioral of L1_T2_LOGIC_EQ is

begin
    --Y <= (((not x(3)) and (not x(2)) and (x(1)) and (x(0))) -- 3 = 0b0011
    --   or ((not x(3)) and (x(2)) and (not x(1)) and (x(0))) -- 5 = 0b0101
    --   or ((x(3)) and (not x(2)) and (x(1)) and (x(0)))); -- 11 = 0b1011
    
    y <= '1' when X = "0011" else
         '1' when X = "0101" else
         '1' when X = "1011" else
         '0';

end Behavioral;
