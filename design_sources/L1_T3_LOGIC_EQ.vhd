----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2022 09:57:09
-- Design Name: 
-- Module Name: L1_T3_LOGIC_EQ - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity L1_T3_LOGIC_EQ is
    Port ( X : in STD_LOGIC_VECTOR (6 downto 0);
           y : out STD_LOGIC);
end L1_T3_LOGIC_EQ;

architecture Behavioral of L1_T3_LOGIC_EQ is

begin
    y <= '0' when X = "0000001" else   -- = 1	
         '0' when X = "0000101" else   -- = 5	
         '0' when X = "0100001" else   -- = 33
         '0' when X = "1000010" else   -- = 66
         '0' when X = "1000100" else   -- = 68
         '0' when X = "1100011" else   -- = 99
         '0' when X = "1101001" else   -- = 105
         '1';

end Behavioral;
