----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.11.2022 9:48:01
-- Design Name: 
-- Module Name: L1_T4_3to7conv - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
--use IEEE.std_logic_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity L1_T4_3to7conv is
    Port ( A : in STD_LOGIC_VECTOR (2 downto 0);
         Y : out STD_LOGIC_VECTOR (6 downto 0));
end L1_T4_3to7conv;

architecture Behavioral of L1_T4_3to7conv is

signal number_to_shift: integer;

begin

    -- principle of wokr: 
    --  for 0 return 0
    --  for rest return Y <= ~(("1111110")<<(decimal(A)-1)); 

    number_to_shift <= to_integer(unsigned(A)-1);
        
    y <= (others=>'0') when A = "000" else
         std_logic_vector(not((not to_unsigned(1, 7)) sll number_to_shift));
    
end Behavioral;
