# VHDL List 1

## Requirements for opening project

- Vivado 2021.2

- Installed tcl scripts for git integration: https://github.com/barbedo/vivado-git

## How to open project using .tcl script

When opening the project after cloning it, do it by using `Tools -> Run Tcl Script...` and selecting the `vhdl-list-1.tcl`.

## How to open project if .tcl does not work

Create new project and just add files in project tree:

`RMB: Design Sources -> Add Sources -> Add Or Create Design Sources -> Add Directories -> choose /desing_sources/ directory.`

and:

`RMB: Simulation Sources -> Add Sources -> Add Or Create Simulation Sources -> Add Directories -> choose /test_benches/ directory.`

## How to commit changes

Follow instruction at https://github.com/barbedo/vivado-git

## Tasks & solutions

Click photo bellow to get the PDF.

[![Tasks](/doc/Tasks.png)](/doc/LISTA_1___FPGA_ENG.pdf)

### Task 1.1

![Task1.1](/doc/Task1.png)

![Task1 solution](/doc/Task1.1_Plot1.png)

### Task 1.2

![Task1.2](/doc/Task1.png)

![Task1 solution](/doc/Task1.2_Plot1.png)

### Task 2

![Task2](/doc/Task2.png)

![Task2 solution](/doc/Task2_Plot1.png)

### Task 3

![Task3](/doc/Task3.png)

![Task3 solution](/doc/Task3_Plot1.png)

### Task 4

![Task4](/doc/Task4.png)

![Task4 solution](/doc/Task4_Plot1.png)

### Task 5

![Task5](/doc/Task5.png)

![Task5 solution](/doc/Task5_Plot1.png)

### Task 6

![Task6](/doc/Task6.png)

![Task6 solution](/doc/Task6_Plot1.png)

## Author of solution

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)

## Contribution

https://vhdl.lapinoo.net/testbench/